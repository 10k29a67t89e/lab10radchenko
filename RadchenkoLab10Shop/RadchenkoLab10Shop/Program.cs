﻿using System;
using System.IO;
using System.Text;
using ClassLibraryShop;
using System.Collections.Generic;
using System.Linq;

namespace RadchenkoLab10Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            // Предметная область: деревенский магазин в котром есть все необходимое.
            // Будем считать, что завоз продуктов происходит раз в неделю, поэтому в задании необходимо отсеять продукты у которых истек срок годности.
            // Результатом будет решение проверяющего нет ли просроченных продуктов в магазинe.
            // Регистрация провайдера кодировок.
            // Делается один раз в приложении. ;
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            // Регистрация кодировки WINDOWS-1251 для поддержки кирилицы.
            Encoding encoding = Encoding.GetEncoding(1251);
            Console.WriteLine("Добро пожаловать в программу!");
            Console.WriteLine("Введите количество недель, которые лежат продукты:");
            Console.WriteLine("Примечание: Будьте внимательны, что продукты питания хранятся не более 12 недель. Если пользователь введет количество недель больше 12, список продуктов питания будет пуст");
            double n = Convert.ToDouble(Console.ReadLine());
            Shop shop = new Shop();
            shop.LoadAll();
            shop.DeleteSpoiledProduct(n);
            Checker checker = new Checker();
            checker.LoadAl();
            checker.Checking();
            Console.WriteLine("Проверка пройдена успешно!");
            Worker worker1 = new Worker(30000, "Алан");
            worker1.Emoution = new List<string>()
            {
                "Премия",
                "Штраф"
            };
            Worker worker2 = new Worker(25000, "Ева");
            worker2.Emoution = new List<string>()
            {
                "Премия",
                "Штраф"
            };
            Worker worker3 = new Worker(32000, "Ричард");
            worker3.Emoution = new List<string>()
            {
                "Премия",
                "Штраф"
            };
            Worker worker4 = new Worker(26000, "Илона");
            worker4.Emoution = new List<string>()
            {
                "Премия",
                "Штраф"
            };
            Worker worker5 = new Worker(20000, "Мухамед");
            worker5.Emoution = new List<string>()
            {
                "Премия",
                "Штраф"
            };
            MailingInf mail = new MailingInf()
            {
                NameM = "Старший менеджер"
            };
            Console.CancelKeyPress += worker1.Console_CancelKeyPress;
            Console.CancelKeyPress += worker2.Console_CancelKeyPress;
            Console.CancelKeyPress += worker3.Console_CancelKeyPress;
            Console.CancelKeyPress += worker4.Console_CancelKeyPress;
            Console.CancelKeyPress += worker5.Console_CancelKeyPress;
            mail.HandlerWithArgsNotify += worker1.ReactionForMailingInf;
            mail.HandlerWithArgsNotify += worker1.ReactionForMailingInf;
            mail.HandlerWithArgsNotify += worker2.ReactionForMailingInf;
            mail.HandlerWithArgsNotify += worker3.ReactionForMailingInf;
            mail.HandlerWithArgsNotify += worker4.ReactionForMailingInf;
            mail.HandlerWithArgsNotify += worker5.ReactionForMailingInf;
            Console.WriteLine("Инструкция по использованию:");
            Console.WriteLine("1) Ответьте хотите ли вы выдать кому-то премию или выписать штраф?");
            Console.WriteLine("2) Ответьте кому именно вы хотите выдать премию либо штраф?");
            Console.WriteLine("Алан, Ричард, Илона, Мухамед, Ева");
            Console.WriteLine("3) Выберите вы хотите выбрать штраф или премию?");
            Console.WriteLine("4) Введите свой выбор в указанные поля, после каждого введенного ответа нажимайте Enter");
            mail.SentInfo($"Вам начислена премия!" , "Премия");
            mail.SentInfo($"С вас списан штраф!", "Штраф");
           
        }
       

    }


    





}