﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
    public class Meats : Goods, ISpoiledProduct
    {
        public string TypeOfMeats { get; set; }
        public int ExpairationDate { get; set; }
        public override string ToExcel()
        {
            return $"{this.Name};{this.Price};{this.TypeOfMeats};{this.ExpairationDate};{this.Quantity}";
        }
    }
}
