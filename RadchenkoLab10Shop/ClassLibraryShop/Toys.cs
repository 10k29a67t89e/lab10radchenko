﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
    public class Toys : Goods
    {
        public int PermissibleAge { get; set; } //допустимый возраст
        public override string ToExcel()
        {
            return $"{this.Name};{this.Price};{this.PermissibleAge};{this.Quantity}";
        }
    }
}
