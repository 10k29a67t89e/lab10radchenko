﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ClassLibraryShop
{
    public class Shop 
    {
        public List<Dairy> Dairyy { get; set; } = new List<Dairy>();
        public void LoadDairy ()
        {
            var pathDairy = @"C:\Users\катя\Documents\lab10radchenko\RadchenkoLab10Shop\ClassLibraryShop\Dairy.csv";
            var lines22 = File.ReadAllLines(pathDairy);
            for (var i = 0; i < lines22.Length-1; i++)
            {

                var splits = lines22[i+1 ].Split(';');
                var ddairy = new Dairy();
                ddairy.Name = splits[0];
                ddairy.Price = Convert.ToInt32(splits[1]);
                ddairy.MilkPercentages = Convert.ToString(splits[2]);
                ddairy.ExpairationDate = Convert.ToInt32(splits[3]);
                ddairy.Quantity = Convert.ToInt32(splits[4]);
                Dairyy.Add(ddairy);
               // Console.WriteLine(ddairy);
            }
        }
        public List<Breads> Breadss { get; set; } = new List<Breads>();
        public void LoadBreads ()
        {
            var pathBread = @"C:\Users\катя\Documents\lab10radchenko\RadchenkoLab10Shop\ClassLibraryShop\Bread.csv";
            var lines3 = File.ReadAllLines(pathBread);
            for (var i = 0; i < lines3.Length-1; i++)
            {
                var splits = lines3[i+1].Split(';');
                var bread = new Breads();
                bread.Name = splits[0];
                bread.Price = Convert.ToInt32(splits[1]);
                bread.TypeOfGrain = Convert.ToString(splits[2]);
                bread.ExpairationDate = Convert.ToInt32(splits[3]);
                bread.Quantity = Convert.ToInt32(splits[4]);
                Breadss.Add(bread);
                //Console.WriteLine();
            }
        }
        public List<Meats> Meatss { get; set; } = new List<Meats>();
        public void LoadMeats()
        {
            var pathMeats = @"C:\Users\катя\Documents\lab10radchenko\RadchenkoLab10Shop\ClassLibraryShop\Meats.csv";
            var lines4 = File.ReadAllLines(pathMeats);
            for (var i = 0; i < lines4.Length-1; i++)
            {
                var splits = lines4[i+1].Split(';');
                var meat = new Meats();
                meat.Name = splits[0];
                meat.Price = Convert.ToInt32(splits[1]);
                meat.TypeOfMeats = Convert.ToString(splits[2]);
                meat.ExpairationDate = Convert.ToInt32(splits[3]);
                meat.Quantity = Convert.ToInt32(splits[4]);
                Meatss.Add(meat);
                //Console.WriteLine();
            }
        }
        public void LoadAll()
        {
            LoadDairy();
            LoadBreads();
            LoadMeats();
        }
            public void DeleteSpoiledProduct(double n)
            {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding encoding = Encoding.GetEncoding(1251);
            //Console.WriteLine($"Введите количество недель, которые лежат продукты:");
            //Console.WriteLine("Примечание: Будьте внимательны, что продукты питания хранятся не более 12 недель. Если пользователь введет количество недель больше 12, список продуктов питания будет пуст");
            //double n = Convert.ToDouble(Console.ReadLine());
            double time = n * 7;
            var searchproduct = from i in Dairyy
                            where i.ExpairationDate > time 
                            select i;
            var result = "Product.csv";
            using (StreamWriter streamWriter = new StreamWriter(result, false, encoding)) 
            {
                streamWriter.WriteLine("Накладная, имеющихся продуктов, с не истекшим сроком годности.");
                streamWriter.WriteLine($"Name;Price;MilkPercentages;ExpairationDate;Quantity;");
                foreach (var x in searchproduct)
                {
                    streamWriter.WriteLine(x.ToExcel());
                }
            }
            var searchproduct2 = from y in Breadss
                            where y.ExpairationDate > time
                            select y;
            using (StreamWriter streamWriter = new StreamWriter(result, true, encoding)) 
            {
                streamWriter.WriteLine();
                streamWriter.WriteLine($"Name;Price;TypeOfGrain;ExpairationDate;Quantity;");
                foreach (var w in searchproduct2)
                {
                    streamWriter.WriteLine(w.ToExcel());
                }
            }
            var searchproduct3 = from z in Meatss
                                 where z.ExpairationDate > time
                                 select z;
            using (StreamWriter streamWriter = new StreamWriter(result, true, encoding)) 
            {
                streamWriter.WriteLine();
                streamWriter.WriteLine($"Name;Price;TypeOfMeats;ExpairationDate;Quantity;");
                foreach (var p in searchproduct3)
                {
                    streamWriter.WriteLine(p.ToExcel());
                }
            }
            Console.WriteLine($"Файл, с продуктами, у которых не истек срок годности создан. Проверено для {time} дней.");
            //Создаем для удобства файл, сoдержащий продукты с испорченным сроком годности.
            var searchdamagetproduct1 = from i in Dairyy
                                       where i.ExpairationDate < time 
                                       select i;
            var result1 = "Damaget Product.csv";
            using (StreamWriter streamWriter = new StreamWriter(result1, false, encoding)) 
            {
                streamWriter.WriteLine($"Name;Price;ExpairationDate;Quantity;");
                foreach (var x in searchdamagetproduct1)
                {
                    streamWriter.WriteLine(x.ToExcel());
                }
            }
            var searchdamagetproduct2 = from i in Breadss
                                        where i.ExpairationDate < time
                                        select i;
            using (StreamWriter streamWriter = new StreamWriter(result1, true, encoding))
            {
                foreach (var x in searchdamagetproduct2)
                {
                    streamWriter.WriteLine(x.ToExcel());
                }
            }
            var searchdamagetproduct3 = from i in Meatss
                                        where i.ExpairationDate < time
                                        select i;
            using (StreamWriter streamWriter = new StreamWriter(result1, true, encoding))
            {
                foreach (var x in searchdamagetproduct3)
                {
                    streamWriter.WriteLine(x.ToExcel());
                }
            }

            
            }
    }
}
