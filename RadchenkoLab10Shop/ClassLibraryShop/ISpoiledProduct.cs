﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace ClassLibraryShop
{
    public interface ISpoiledProduct
    {
        int ExpairationDate { get; set; }
    }
}
