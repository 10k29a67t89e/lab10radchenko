﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
     public class HomeChemistry:Goods
    {
        public string Consistence { get; set; }
        public override string ToExcel()
        {
            return $"{this.Name};{this.Price};{this.Consistence};{this.Quantity}";
        }
    }
}
