﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
    public class Checker
    { 
            Encoding encoding = Encoding.GetEncoding(1251);
        public List<Toys> Toyss { get; set; } = new List<Toys>();
        public List<HomeChemistry> HomeChemistries { get; set; } = new List<HomeChemistry>();
        public void LoadToys()
        {
            var pathToys = @"C:\Users\катя\Documents\lab10radchenko\RadchenkoLab10Shop\ClassLibraryShop\Toys.csv";
            var lines1 = File.ReadAllLines(pathToys);
            for (var i = 0; i < lines1.Length-1; i++)
            {
                var splits = lines1[i+1].Split(';');
                var toy = new Toys();
                toy.Name = splits[0];
                toy.Price = Convert.ToInt32(splits[1]);
                toy.PermissibleAge = Convert.ToInt32(splits[2]);
                toy.Quantity = Convert.ToInt32(splits[3]);
                Toyss.Add(toy);
                //Console.WriteLine();
            }
        }
        public void LoadHomeChemistry()
        {
            var pathHomechemistry = @"C:\Users\катя\Documents\lab10radchenko\RadchenkoLab10Shop\ClassLibraryShop\HomeChemistry.csv";
            var lines5 = File.ReadAllLines(pathHomechemistry);
            for (var i = 0; i < lines5.Length-1; i++)
            {
                var splits = lines5[i+1].Split(';');
                var chemistry = new HomeChemistry();
                chemistry.Name = splits[0];
                chemistry.Price = Convert.ToInt32(splits[1]);
                chemistry.Consistence = splits[2];
                chemistry.Quantity = Convert.ToInt32(splits[3]);
                HomeChemistries.Add(chemistry);
                //Console.WriteLine();
            }
        }
        public void LoadAl()
        {
            LoadToys();
            LoadHomeChemistry();
        }
        public void Checking()
        {
            Console.WriteLine("Хотите ли вы составить документ со всеми товарами, находящимися в Магазине?");
            string n = Convert.ToString(Console.ReadLine());
            switch (n)
            {
                case "да":
                    var path = @"C:\Users\катя\Documents\lab10radchenko\RadchenkoLab10Shop\RadchenkoLab10Shop\bin\Debug\net5.0\Product.csv";
                    var lines = File.ReadAllLines(path);
                    var result = "Product.csv";
                    using (StreamWriter streamWriter = new StreamWriter(result, true, encoding))//true записывает в исходный список, а false очищает и записывает 
                    {
                        streamWriter.WriteLine();
                        streamWriter.WriteLine($"Name;Price;Consistence;Quantity;");

                        foreach (var x in HomeChemistries)
                        {
                            streamWriter.WriteLine(x.ToExcel());
                        }
                    }
                    using (StreamWriter streamWriter = new StreamWriter(result, true, encoding))//true записывает в исходный список, а false очищает и записывает 
                    {
                        streamWriter.WriteLine();
                        streamWriter.WriteLine($"Name;Price;PermissibleAge;Quantity;");

                        foreach (var i in Toyss)
                        {
                            streamWriter.WriteLine(i.ToExcel());
                        }
                    }
                    break;
                case "нет":
                    Console.WriteLine("Проверка завершена.");
                    break;
            }
            
            

        }
    }
}


