﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClassLibraryShop
{
    public class Breads : Goods, ISpoiledProduct
    {
        public string TypeOfGrain { get; set; }//тип зерна

        public int ExpairationDate { get; set; }
        public override string ToExcel()
        {
            return $"{this.Name};{this.Price};{this.TypeOfGrain};{this.ExpairationDate};{this.Quantity}";
        }
    }
}
