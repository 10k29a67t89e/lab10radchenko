﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
     public class Goods
    {
        public int Price { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

        // https://docs.microsoft.com/ru-ru/dotnet/csharp/language-reference/keywords/virtual
        public virtual string ToExcel()
        {
            return $"{this.Name},{this.Price},{this.Quantity}";
        }
    }
}
