﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
   public  class WorkerEventArgs
    {
        public string Message { get; set; }
        public string Reason { get; set; }
        public string Answer { get; set; }
        public string name { get; set; }
        public int get { get; set; }
        public string yes { get; set; }
        public WorkerEventArgs(string message ="", string reason ="")
        {
            Message = message;
            Reason = reason;
        }
    }
}
