﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
    public class Dairy : Goods, ISpoiledProduct // молочные продукты
    {
        public string MilkPercentages { get; set; } //процентное содержание молока в продукте
        public int ExpairationDate { get; set; }
        public override string ToExcel()
        {
            return $"{this.Name};{this.Price};{this.MilkPercentages};{this.ExpairationDate};{this.Quantity}";
        }
    }
}
