﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryShop
{
    public class MailingInf
    {
        public delegate void HandlerWithArgs(object sender, WorkerEventArgs e);
        public string NameM { get; set; }
        public event HandlerWithArgs? HandlerWithArgsNotify;
        public void SentInfo(string message, string reason)
        {
            HandlerWithArgsNotify?.Invoke(this, new WorkerEventArgs(message, reason));
        }
    }
}
